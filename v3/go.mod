module modernc.org/cc/v3

go 1.19

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/google/go-cmp v0.5.9
	lukechampine.com/uint128 v1.2.0
	modernc.org/mathutil v1.6.0
	modernc.org/strutil v1.2.0
	modernc.org/token v1.1.0
)

require github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
